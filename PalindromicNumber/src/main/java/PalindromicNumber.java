import java.util.Scanner;

/**
 * Created by HaChi on 12/16/2014.
 */
public class PalindromicNumber {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int T = input.nextInt();
        input.nextLine();

        Integer[] word = new Integer[2*T];
        for (int i = 0; i < 2*T; i++) {
            word[i] = input.nextInt();
        }

       for(int i = 0;i < 2*T; i+=2){
           System.out.println(NumberOfPalindromicNumber(word[i], word[i+1]));
        }
    }
    public static int NumberOfPalindromicNumber(Integer A, Integer B){
        Integer count = 0;
       for (Integer i = A; i <= B; i++){
            String reverse = new StringBuilder(String.valueOf(i)).reverse().toString();
            Integer  reversed = Integer.parseInt(reverse);
            if(Integer.compare(i , reversed) == 0){
                count ++;
            }

        }
        return count;
    }
}

